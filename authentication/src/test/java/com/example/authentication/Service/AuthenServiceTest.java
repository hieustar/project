package com.example.authentication.Service;

import com.example.authentication.Model.Request.AuthenRequest;
import com.example.authentication.Model.Request.LoginAccountRequest;
import com.example.authentication.config.JwtUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.http.*;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.MockServerClientHttpResponse;
import org.springframework.web.client.RestTemplate;
import redis.clients.jedis.Jedis;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

/**
 * @author hieuvm
 */
@RunWith(SpringRunner.class)
public class AuthenServiceTest {
    @MockBean
    RestTemplate mockRestTemplate;

    @MockBean
    JwtUtil jwtUtil;
    @InjectMocks
    AuthenService authenService;
    @MockBean
    Jedis jedis;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

    }
    @Test
    public void checkLoginTest() throws Exception {
        LoginAccountRequest request= new LoginAccountRequest("vmhieu","ite");
        AuthenRequest authenRequest= new AuthenRequest("vmhieu","ite","ROLE_ADMIN");
        UserDetails user = new User("vmhieu", "ite", Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN")));
        Mockito.when(mockRestTemplate
                .exchange(Mockito.eq("http://localhost:8081/general/login"), Mockito.any(), Mockito.any(), Mockito.eq(AuthenRequest.class)))
                .thenReturn(new ResponseEntity<>(authenRequest, HttpStatus.OK));

        Mockito.when(jwtUtil.generateToken(user)).thenReturn("token");
        //Mockito.when(jedis.set(user.getUsername(),"toekn")).thenReturn("");
        ResponseEntity responseEntity= authenService.checkLogin(request);
        Assert.assertNotNull(responseEntity);
        Assert.assertNotNull(responseEntity.getBody());
        Assert.assertNotNull(responseEntity.getStatusCode());
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Accept", "application/json");
        return headers;
    }

    private String getBody(final Object user) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(user);
    }

}