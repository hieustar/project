package com.example.authentication;


import com.example.authentication.Model.Request.AuthenRequest;
import com.example.authentication.Model.Request.LoginAccountRequest;
import com.example.authentication.Service.AuthenService;
import com.example.authentication.config.JwtUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@SpringBootTest
@RunWith(Runner.class)
public class AuthenticationApplicationTests {
//	@Test
//	public void testAddEmployeeSuccess() throws URISyntaxException {
//		final String baseUrl = "http://localhost:8081/general/register";
//		URI uri = new URI(baseUrl);
//		Account account= new Account(null,"minh hieu","tb","abc","mvmh","$2a$10$BWyhFi81jA2UBO7VH5mZEu.pkGcJMcSBF2thL1WOSx5Xf2l7L1Xiq","ROLE_ADMIN");
//		HttpHeaders headers = new HttpHeaders();
//		headers.set("Content-Type", "application/json");
//		headers.set("Accept", "application/json");
//
//		HttpEntity<Account> request = new HttpEntity<>(account, headers);
//
//		Account result = this.testRestTemplate.postForObject(baseUrl, request, Account.class);
//
//		//Verify request succeed
//		Assert.assertEquals(201, result);
//	}

//    @MockBean
//    RestTemplate mockRestTemplate;
//    @MockBean
//    JwtUtil jwtUtil;
//    @InjectMocks
//    AuthenService authenService;
//
//    @Before
//    public void init() {
//        MockitoAnnotations.initMocks(this);
//
//    }
//    @Test
//    public void checkLoginTestt() throws Exception {
//        LoginAccountRequest request= new LoginAccountRequest("test","test");
//        AuthenRequest authenRequest= new AuthenRequest("test","test","ROLE_ADMIN");
//        UserDetails user = new User("test", "test", Collections.singleton(new SimpleGrantedAuthority("ROLE_ADMIN")));
//        Mockito.when(mockRestTemplate.exchange(Mockito.eq("http://localhost:8082/general/login"), Mockito.any(), Mockito.any(), Mockito.eq(AuthenRequest.class)))
//                .thenReturn(new ResponseEntity<>(authenRequest, HttpStatus.OK));
//        Mockito.when(jwtUtil.generateToken(user)).thenReturn("token");
//        ResponseEntity responseEntity= authenService.checkLogin(request);
//        Assert.assertNotNull(responseEntity);
//        Assert.assertNotNull(responseEntity.getBody());
//        Assert.assertNotNull(responseEntity.getStatusCode());
//        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//    }


	@Test
	void contextLoads() {
	}

}
