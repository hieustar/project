package com.example.authentication.Model.Request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author hieuvm
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountRequest {
    @SerializedName("full_name")
    @JsonProperty("full_name")
    String fullName;
    @SerializedName("address")
    @JsonProperty("address")
    String address;
    @SerializedName("link_img")
    @JsonProperty("link_img")
    String linkImg;
    @JsonProperty("username")
    @SerializedName("username")
    String userName;
    @SerializedName("password")
    @JsonProperty("password")
    String passWord;
    @SerializedName("role")
    @JsonProperty("role")
    String role;
}
