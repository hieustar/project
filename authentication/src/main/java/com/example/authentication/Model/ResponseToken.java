package com.example.authentication.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hieuvm
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseToken {
    @JsonProperty("token")
    @SerializedName("token")
    private String token;

}
