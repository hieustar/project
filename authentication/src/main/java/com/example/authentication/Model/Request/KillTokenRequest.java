package com.example.authentication.Model.Request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hieuvm
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KillTokenRequest {
    @JsonProperty("username")
    @SerializedName("username")
    String userName;
}
