package com.example.authentication.Service;

import com.example.authentication.Model.Request.AuthenRequest;
import com.example.authentication.Model.Request.KillTokenRequest;
import com.example.authentication.Model.Request.LoginAccountRequest;
import com.example.authentication.Model.ResponseToken;
import com.example.authentication.config.JwtUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import redis.clients.jedis.Jedis;

import java.util.Arrays;
import java.util.List;

/**
 * @author hieuvm
 */
@Service
public class AuthenService{

    @Autowired
    JwtUtil jwtUtil;
    @Autowired
    PasswordEncoder bcryptEncoder;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    AuthenService authenService;
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    Jedis jedis;
    @Autowired
    RestTemplate restTemplate;

    public ResponseEntity<?> checkLogin(LoginAccountRequest accountRequest) throws Exception {

        String bodyLogin= getBody(accountRequest);
        HttpHeaders headers= getHeaders();
        HttpEntity<String> entityLogin= new HttpEntity<>(bodyLogin,headers);
        ResponseEntity<AuthenRequest> responseEntityLogin= restTemplate.exchange("http://localhost:8081/general/login", HttpMethod.POST,
                entityLogin,AuthenRequest.class);
        if (responseEntityLogin.getStatusCode().equals(HttpStatus.OK)) {
            AuthenRequest authenRequest = responseEntityLogin.getBody();
//            Authentication authentication = authenticationManager.authenticate(
//                    new UsernamePasswordAuthenticationToken(
//                            authenRequest.getUserName(),
//                            authenRequest.getPassWord()
//                    )
//            );
//            SecurityContextHolder.getContext().setAuthentication(authentication);

            List<SimpleGrantedAuthority> roles = Arrays.asList(new SimpleGrantedAuthority(authenRequest.getRole()));
            UserDetails userDetails = new User(authenRequest.getUserName(), authenRequest.getPassWord(), roles);
            String genToken = jwtUtil.generateToken(userDetails);
            String token = "Bearer " + genToken;
            jedis.set(userDetails.getUsername(),token);
            ResponseToken responseToken= new ResponseToken(token);
            String bodyToken= getBody(responseToken);
            HttpHeaders tokenHeaders = getHeaders();
            tokenHeaders.set("Authorization", token);
            HttpEntity<String> tokenEntity = new HttpEntity<>(bodyToken,headers);
            //send token
        //ResponseEntity<String> tokenResponse= restTemplate1.exchange("http://localhost:8081/general/get-token", HttpMethod.POST, tokenEntity, String.class);
        return ResponseEntity.ok(token);
        }

            return ResponseEntity.ok("No content");
    }


    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Accept", "application/json");
        return headers;
    }

    private String getBody(final Object user) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(user);
    }

    public String actionKillToken(KillTokenRequest killTokenRequest) {
        String userName= killTokenRequest.getUserName();
        jedis.del(userName);
        return "Success";
    }
}
