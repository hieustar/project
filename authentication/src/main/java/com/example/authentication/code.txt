HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add(applicationConfig.getNvTokenHeader(), token);
        HttpEntity<Map> requestForm = new HttpEntity<>(form, headers);
        Map<String, Object> body = (Map<String, Object>) ((Map<String, Object>) restTemplate.postForEntity(applicationConfig.getGetInfoUrl(), requestForm, Map.class).getBody().get("data")).get("user");
Mockito.when(restTemplate.exchange(Mockito.eq("http://localhost:8000/general/login"), Mockito.any(), Mockito.any(), Mockito.eq(AuthenRequest.class)))
.thenReturn(new ResponseEntity<>(authenRequest, HttpStatus.OK));