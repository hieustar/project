package com.example.authentication.Controller;

import com.example.authentication.Model.Request.AccountRequest;
import com.example.authentication.Model.Request.KillTokenRequest;
import com.example.authentication.Model.Request.LoginAccountRequest;
import com.example.authentication.Service.AuthenService;
import com.example.authentication.config.JwtUtil;
import com.example.authentication.db.Account;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * @author hieuvm
 */
@RequestMapping("/authen")
@RestController
public class GeneralController {
    @Autowired
    AuthenService authenService;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginAccountRequest accountRequest) throws Exception {
        return ResponseEntity.ok(authenService.checkLogin(accountRequest));
    }
    @PostMapping(path = "/kill-token")
    public String killToken(@RequestBody KillTokenRequest killTokenRequest){
        return authenService.actionKillToken(killTokenRequest);

    }


    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        headers.set("Accept", "application/json");
        return headers;
    }

    private String getBody(final Object user) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(user);
    }
    @PostMapping(path = "/test")
    public String test(){
        return "hello";
    }

}
