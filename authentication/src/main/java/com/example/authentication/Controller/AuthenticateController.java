package com.example.authentication.Controller;

import com.example.authentication.Model.Request.AccountRequest;
import com.example.authentication.Service.AuthenService;
import com.example.authentication.db.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author hieuvm
 */
@RestController
public class AuthenticateController {
    @Autowired
    AuthenService authenService;
    @PostMapping("/hellouser")
    public String getUser()
    {
        return "Hello User";
    }

    @PostMapping("/helloadmin")
    public String getAdmin()
    {
        return "Hello Admin";
    }

}
