package com.example.authentication.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.client.RestTemplate;
import redis.clients.jedis.Jedis;

/**
 * @author hieuvm
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
    //extends WebSecurityConfigurerAdapter

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception
    {
        return super.authenticationManagerBean();
    }
@Override
public void configure(final HttpSecurity http) throws Exception {
    http.csrf().disable();
    http.headers().httpStrictTransportSecurity().disable();
    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

    // Authorize sub-folders permissions
   // http.antMatcher("/authen/*").authorizeRequests().anyRequest().permitAll();
    http.antMatcher("/login").authorizeRequests().anyRequest().permitAll();
}

    @Bean
    public Jedis jedis(){
        return new Jedis();
    }
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

}
