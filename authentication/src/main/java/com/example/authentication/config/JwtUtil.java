package com.example.authentication.config;

import com.example.authentication.config.mongo.ResourceProperties;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author hieuvm
 */
@Service
public class JwtUtil {
    @Autowired
    ResourceProperties resourceProperties;


    public String generateToken(UserDetails userDetails){
        Map<String,Object> claims= new HashMap<>();
        Collection<? extends GrantedAuthority> roles=userDetails.getAuthorities();
        if (roles.contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
            claims.put("isAdmin", true);
        }
        if (roles.contains(new SimpleGrantedAuthority("ROLE_USER"))) {
            claims.put("isUser", true);
        }

        return doGenerateToken(claims, userDetails.getUsername());
    }
    private String doGenerateToken(Map<String, Object> claims, String subject){
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis()+resourceProperties.getJwtExpirationInMs()))
                .signWith(SignatureAlgorithm.HS512,resourceProperties.getSecret()).compact();
    }

}
