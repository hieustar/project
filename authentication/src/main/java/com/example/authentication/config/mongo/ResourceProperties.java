package com.example.authentication.config.mongo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author hieuvm
 */
@Configuration
@Data
public class ResourceProperties {
    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.expirationDateInMs}")
    private int jwtExpirationInMs;
//    @Value("${spring.data.mongodb.database}")
//    private String dbname;
}
