package com.example.account.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author hieuvm
 */
class JwtUtilTest {

    @Autowired
    CustomJwtAuthenticationFilter customJwtAuthenticationFilter;
    @Autowired
    JwtUtil jwtUtil;
    @Test
    void validateToken() throws ServletException, IOException {
//        String jwt = "wrong_jwt";
//        MockHttpServletRequest request = new MockHttpServletRequest();
//        request.addHeader("Authorization", "Bearer " + jwt);
//        request.setRequestURI("/authenticate/test");
//        MockHttpServletResponse response = new MockHttpServletResponse();
//        MockFilterChain filterChain = new MockFilterChain();
//        customJwtAuthenticationFilter.doFilter(request, response, filterChain);
//        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
//        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();

    }

    @Test
    void getUsernameFromToken() {
        Claims claims= Jwts.parser().setSigningKey("vmh").parseClaimsJws("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ2bWhpZXUiLCJpc0FkbWluIjp0cnVlLCJleHAiOjE2MjQwMjM2NzgsImlhdCI6MTYyNDAwNTY3OH0.4tXN2zTyfKe-Xqv3fUucqg3m0hgo5ffepKvOJe6W6J_LLVLUEsDgjxQ1z_I0kxFuvyr585pf2tjbtNQ-Ac-YaA").getBody();
        assertThat(claims.getSubject()).isEqualTo("vmhieu");
        //return claims.getSubject();
    }

    @Test
    void getRolesFromToken() {
    }
    @Test
    public void testReturnFalseWhenJWTisMalformed() {
        String token= "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ2bWhpZXUiLCJpc0FkbWluIjp0cnVlLCJleHAiOjE2MjQwMjM2NzgsImlhdCI6MTYyNDAwNTY3OH0.4tXN2zTyfKe-Xqv3fUucqg3m0hgo5ffepKvOJe6W6J_LLVLUEsDgjxQ1z_I0kxFuvyr585pf2tjbtNQ-Ac-YaA";
        String isInvalid= token.substring(3);
        boolean isTokenInvalid= jwtUtil.validateToken(isInvalid);
        assertThat(isTokenInvalid).isEqualTo(false);
    }
    @Test
    public void testReturnFalseWhenJWTisExpireTime(){

    }
    private Authentication createAuthentication() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        return new UsernamePasswordAuthenticationToken("anonymous", "anonymous", authorities);
    }
}