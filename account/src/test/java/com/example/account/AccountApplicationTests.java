//package com.example.account;
//
//import static org.junit.Assert.assertEquals;
//import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
//import com.example.account.Repository.AccountRepo;
//import com.example.account.Service.AuthenService;
//import com.example.account.db.Account;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.Before;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//@RunWith(SpringRunner.class)
//@SpringBootTest
//class AccountApplicationTests {
//	@Autowired
//	AuthenService authenService;
//	@MockBean
//	AccountRepo accountRepo;
//	private MockMvc mockMvc;
//	@Autowired
//	private WebApplicationContext context;
//	@Autowired
//	private ObjectMapper mapper;
//
//	@Before
//	public void setup() {
//		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
//	}
//	@Test
//	@WithMockUser("/example")
//	public void saveAccountTest() throws Exception {
//		Account account= new Account(null,"minh hieu","tb","abc","mvmh","$2a$10$BWyhFi81jA2UBO7VH5mZEu.pkGcJMcSBF2thL1WOSx5Xf2l7L1Xiq","ROLE_ADMIN");
//		String jsonRequest = mapper.writeValueAsString(account);
//		MvcResult result = mockMvc
//				.perform(post("http://localhost:8081/general/register").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk()).andReturn();
//
//		assertEquals(200, result.getResponse().getStatus());
//	}
//	@Test
//	public void testGetAll(){
//		when(accountRepo.findAll()).thenReturn(Stream.of(
//				new Account("60bc56e16257f7038e09c254","Vu minh hieu","tb","abc","vmhieu","$2a$10$BWyhFi81jA2UBO7VH5mZEu.pkGcJMcSBF2thL1WOSx5Xf2l7L1Xiq","ROLE_ADMIN")
//				,new Account("60bca1452b522204f0f4bfdf","Vu minh hieu hieu","tb","abc","vmhieu","$2a$10$EhZSxqs9L3Kkcf2Y/MPW2et0VXjfW0zCTtDnybEQl1mLz38tgZ3Se","ROLE_USER")
//		).collect(Collectors.toList()));
//		assertEquals(2,authenService.getAllTest().size());
//	}
//
//
//}
