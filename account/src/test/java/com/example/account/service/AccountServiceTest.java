package com.example.account.service;

import com.example.account.Repository.AccountRepo;
import com.example.account.Service.AuthenService;
import com.example.account.db.Account;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

//import static org.junit.Assert.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.doReturn;

/**
 * @author hieuvm
 */
public class AccountServiceTest {
    private AccountRepo accountRepoTest= Mockito.mock(AccountRepo.class);
    @Autowired
    private AuthenService authenServiceTest;
    @BeforeEach
    void init(){
        Account account= new Account("12345","minh hieu","tb","abc","mvmh","$2a$10$BWyhFi81jA2UBO7VH5mZEu.pkGcJMcSBF2thL1WOSx5Xf2l7L1Xiq","ROLE_ADMIN");
        doReturn(Optional.of(account)).when(accountRepoTest).findById("");
        List<Account> accounts= Arrays.asList(account);
        doReturn(accounts).when(accountRepoTest).findAll();
    }
    @DisplayName("")
    @Test
    public void findAll(){
//        List<Account> accountList= (List<Account>) authenServiceTest.getAllTest();
//        Aser
//        assertThat(accountList).hasSize(2);
    }
}
