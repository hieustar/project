package com.example.account.service;

import com.example.account.Model.Request.AccountRequest;
import com.example.account.Model.Request.AuthenRequest;
import com.example.account.Model.Request.ChangePasswordAccountRequest;
import com.example.account.Model.Request.LoginAccountRequest;
import com.example.account.Repository.AccountRepo;
import com.example.account.Service.AuthenService;
import com.example.account.cache.CacheToken;
import com.example.account.config.CustomJwtAuthenticationFilter;
import com.example.account.db.Account;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenServiceTest {

    @Mock
    AccountRepo accountRepo;
    @InjectMocks
    AuthenService authenService = new AuthenService(accountRepo);
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    AuthenticationManager authenticationManager;
    @Mock
    CacheToken cacheToken;
    @Mock
    Jedis jedis;
    @Mock
    RedisTemplate redisTemplate;
    @Mock
    CustomJwtAuthenticationFilter customJwtAuthenticationFilter;
    LoginAccountRequest loginAccountRequest;
    LoginAccountRequest loginAccountRequestFail;
    MockHttpServletRequest request;
    Account account;
    @Mock
    AuthenService authenServices;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        request= new MockHttpServletRequest();
        request.addHeader("Authorization","Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ2bWhpZXUiLCJpc0FkbWluIjp0cnVlLCJleHAiOjE2MjM3NDY4OTAsImlhdCI6MTYyMzcyODg5MH0.A5Fy574PWIg3Lyq2Jaiq44F5hdAyyjCO9_CjHCmdJNM-e5sOLOEb4087Lq--sALphRQmslMH892ARghA0m3wHA");
        loginAccountRequest = new LoginAccountRequest();
        loginAccountRequest.setUserName("test");
        loginAccountRequest.setPassWord(passwordEncoder.encode("test"));

        loginAccountRequestFail= new LoginAccountRequest();
        loginAccountRequestFail.setUserName("abc");
        loginAccountRequestFail.setPassWord(passwordEncoder.encode("abc"));
        account = new Account();
        account.setUserName("test");
        account.setPassWord(passwordEncoder.encode("test"));
        account.setAddress("test");
        account.setFullName("test");
        account.setRole("test");
        account.setId("test");
        account.setLinkImg("test");

        Mockito.when(accountRepo.findByUsername(loginAccountRequest.getUserName())).thenReturn(account);
        Mockito.when(passwordEncoder.matches(loginAccountRequest.getPassWord(),account.getPassWord())).thenReturn(true);



        //Mockito.when(accountRepo.findAll()).thenReturn();
       // Mockito.when(customJwtAuthenticationFilter.returnUsernameFromToken(request)).thenReturn("test");

//        doReturn(account).when(accountRepo).findByUsername("test");
      //  account.setId("test");
    }

    @Test
    public void LoginSuccess() throws Exception {
        ResponseEntity<AuthenRequest> responseEntity = authenService.login(loginAccountRequest);
        Assert.assertNotNull(responseEntity.getBody());
        Assert.assertNotNull(responseEntity.getStatusCode());
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        AuthenRequest request = responseEntity.getBody();
        Assert.assertEquals("test", request.getUserName());

    }
    @Test
    public void LoginFail() throws Exception {
        Mockito.when(accountRepo.findByUsername(loginAccountRequestFail.getUserName())).thenReturn(account);
        Mockito.when(!passwordEncoder.matches(loginAccountRequestFail.getPassWord(),account.getPassWord())).thenReturn(false);
        ResponseEntity<AuthenRequest> responseEntity= authenService.login(loginAccountRequestFail);
       // Assert.assertNull(responseEntity);
        Assert.assertNotNull(responseEntity.getBody());
        Assert.assertNotNull(responseEntity.getStatusCode());
        Assert.assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
        AuthenRequest request= responseEntity.getBody();
        Assert.assertEquals(new AuthenRequest(),request);
    }
    @Test
    public void getAll(){
        List<Account> accounts= new ArrayList<>();
        when(accountRepo.findAll()).thenReturn(accounts);
        List<Account> returned= authenService.getAll();
        Assert.assertEquals(accounts,returned);
    }
    @Test
    public void update(){
        AccountRequest accountUpdate= new AccountRequest();
        Account account1= new Account("test","test","test","test","test",passwordEncoder.encode("test"),"test");
        //accountUpdate.setId("test");
        accountUpdate.setFullName("test");
        accountUpdate.setAddress("test");
        accountUpdate.setUserName("test");
        accountUpdate.setRole("test");
        accountUpdate.setLinkImg("test");
        accountUpdate.setPassWord(passwordEncoder.encode("test"));
        when(accountRepo.findById("test")).thenReturn(java.util.Optional.of(account1));
        Account returned= authenService.update("test",accountUpdate);
        Assert.assertEquals(account1,returned);
    }
    @Test
    public void updateWhenReturnNull(){
        AccountRequest accountUpdate= new AccountRequest();
        Account account1= new Account("test","test","test","test","test",passwordEncoder.encode("test"),"test");
        //accountUpdate.setId("test");
        accountUpdate.setFullName("test");
        accountUpdate.setAddress("test");
        accountUpdate.setUserName("test");
        accountUpdate.setRole("test");
        accountUpdate.setLinkImg("test");
        accountUpdate.setPassWord(passwordEncoder.encode("test"));
        when(accountRepo.findById("abc")).thenReturn(Optional.of(account1));
        Account returned= authenService.update("test",accountUpdate);
        Assert.assertNull(returned);
    }
    @Test
    public void save(){
        Account account3= new Account("test","test","test","test",passwordEncoder.encode("test"),"test");
        AccountRequest accountRequest= new AccountRequest();
        accountRequest.setFullName("test");
        accountRequest.setAddress("test");
        accountRequest.setUserName("test");
        accountRequest.setRole("test");
        accountRequest.setLinkImg("test");
        accountRequest.setPassWord(passwordEncoder.encode("test"));
        //doReturn(true).when(!accountRepo.existsAccountByUsername(accountRequest.getUserName()));
        Mockito.when(accountRepo.existsAccountByUsername("test")).thenReturn(false);
        Mockito.when(accountRepo.save(account3)).thenReturn(account3);
        Account returned= authenService.register(accountRequest);
        Assert.assertNotNull(returned);
    }
    @Test
    public void saveWhenReturnNull(){
        Account account3= new Account("test","test","test","test","test",passwordEncoder.encode("test"),"test");
        AccountRequest accountRequest= new AccountRequest();
        accountRequest.setFullName("test");
        accountRequest.setAddress("test");
        accountRequest.setUserName("test");
        accountRequest.setRole("test");
        accountRequest.setLinkImg("test");
        accountRequest.setPassWord(passwordEncoder.encode("test"));
        //doReturn(true).when(!accountRepo.existsAccountByUsername(accountRequest.getUserName()));
        Mockito.when(accountRepo.existsAccountByUsername("test")).thenReturn(true);
       // Mockito.when(accountRepo.save(account3)).thenReturn(account3);
        Account returned= authenService.register(accountRequest);
        Assert.assertNull(returned);
    }
    @Test
    public void loadUserByUserName(){
        Account account2= new Account("test","test","test","test","test","test","test");
        List<SimpleGrantedAuthority> roles= Arrays.asList(new SimpleGrantedAuthority(account2.getRole()));
        UserDetails userDetails= new User("test","test",roles);
        when(accountRepo.findByUsername(account2.getUserName())).thenReturn(account2);
        UserDetails details=  authenService.loadUserByUsername(account2.getUserName());
        Assert.assertEquals(userDetails,details);
    }
    @Test
    public void changePasswordTest(){
        Account account4= new Account("test","test","test","test","test",passwordEncoder.encode("test"),"test");
        Optional<Account> optionalAccount= Optional.of(account4);
        ChangePasswordAccountRequest request= new ChangePasswordAccountRequest("abc");
        Mockito.when(accountRepo.findById(account4.getId())).thenReturn(optionalAccount);
        Mockito.when(accountRepo.save(account4)).thenReturn(account4);

        Account returned= authenService.changePassword("test",request);
        Assert.assertNotNull(returned);
    }
    @Test
    public void changePasswordWhenReturnNull(){
        Account account4= new Account("test","test","test","test","test",passwordEncoder.encode("test"),"test");
        Optional<Account> optionalAccount= Optional.of(account4);
        ChangePasswordAccountRequest request= new ChangePasswordAccountRequest("abc");
        Mockito.when(accountRepo.findById("account4.getId()")).thenReturn(optionalAccount);
        //Mockito.when(accountRepo.save(account4)).thenReturn(account4);

        Account returned= authenService.changePassword("test",request);
        Assert.assertNull(returned);
    }
}