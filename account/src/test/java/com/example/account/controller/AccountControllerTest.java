//package com.example.account.controller;
//
//import com.example.account.Service.AuthenService;
//import com.example.account.db.Account;
//import org.junit.Test;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.DisplayName;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.hamcrest.Matchers.hasSize;
//import static org.mockito.BDDMockito.given;
//import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
///**
// * @author hieuvm
// */
//@SpringBootTest()
//public class AccountControllerTest {
//    @Autowired
//    private WebApplicationContext context;
//    MockMvc mockMvc;
//    @MockBean
//    AuthenService authenService;
//    private Account account1;
//    private Account account2;
//    private List<Account> accounts;
//    @BeforeEach
//    public void setup(){
//        mockMvc= MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
//        account1=
//                new Account("123","minh hieu","tb","abc","user","$2a$10$BWyhFi81jA2UBO7VH5mZEu.pkGcJMcSBF2thL1WOSx5Xf2l7L1Xiq","ROLE_ADMIN");
//        account2=
//                new Account("456","minh hieu","tb","abc","admin","$2a$10$BWyhFi81jA2UBO7VH5mZEu.pkGcJMcSBF2thL1WOSx5Xf2l7L1Xiq","ROLE_ADMIN");
//        accounts= new ArrayList<>();
//        accounts.add(account1);
//        accounts.add(account2);
//    }
//    @WithMockUser(value = "ADMIN",roles = {"ADMIN"})
//    @DisplayName("Find all")
//    @Test
//    public void testFindAll() throws Exception {
//        given(authenService.getAllTest()).willReturn(accounts);
//        mockMvc.perform(get("http://localhost:8081/general/get-all-test")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$",hasSize(2)));
//    }
//    //Account account= new Account(null,"minh hieu","tb","abc","mvmh","$2a$10$BWyhFi81jA2UBO7VH5mZEu.pkGcJMcSBF2thL1WOSx5Xf2l7L1Xiq","ROLE_ADMIN");
//}
