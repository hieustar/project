package com.example.account.Service;

import com.example.account.Model.Request.*;
import com.example.account.Repository.AccountRepo;
import com.example.account.cache.CacheToken;
import com.example.account.config.CustomJwtAuthenticationFilter;
import com.example.account.db.Account;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import redis.clients.jedis.Jedis;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author hieuvm
 */
@Service
public class AuthenService implements UserDetailsService {
    public AuthenService (AccountRepo accountRepo){
        this.accountRepo=accountRepo;
    }
    @Autowired
    AccountRepo accountRepo;
    @Autowired
    AuthenService authenService;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    CacheToken cacheToken;
    @Autowired
    Jedis jedis;
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    CustomJwtAuthenticationFilter customJwtAuthenticationFilter;
    RestTemplate restTemplate;
    public ResponseEntity<AuthenRequest> login(LoginAccountRequest loginRequest) throws Exception {
        restTemplate= new RestTemplate();

        Account account= accountRepo.findByUsername(loginRequest.getUserName());
        if (passwordEncoder.matches(loginRequest.getPassWord(), account.getPassWord())) {
//            try {
//                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
//                        loginRequest.getUserName(), loginRequest.getPassWord()));
//            } catch (DisabledException e) {
//                throw new Exception("USER_DISABLED", e);
//            } catch (BadCredentialsException e) {
//                throw new Exception("INVALID_CREDENTIALS", e);
//            }
            authenService.loadUserByUsername(account.getUserName());
            AuthenRequest request = new AuthenRequest(account.getUserName(), account.getPassWord(), account.getRole());
            return ResponseEntity.ok(request);
        }else return ResponseEntity.ok(new AuthenRequest());
    }
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        List<SimpleGrantedAuthority> roles = null;
        Account account= accountRepo.findByUsername(s);
        if (account != null) {
            roles = Arrays.asList(new SimpleGrantedAuthority(account.getRole()));
            return new User(account.getUserName(), account.getPassWord(), roles);
        }
        throw new UsernameNotFoundException("User not found with the name " + s);
    }
    public List <Account> getAll(){
//        String bearerToken= request.getHeader("Authorization");
//        String userName= customJwtAuthenticationFilter.returnUsernameFromToken(request);
//        if (bearerToken.equals(jedis.get(userName))) {
//            return accountRepo.findAll();
//        }else return null;
        return accountRepo.findAll();
    }

    public Account update(String id, AccountRequest accountRequest){
//        String bearerToken= request.getHeader("Authorization");
//        String userName= customJwtAuthenticationFilter.returnUsernameFromToken(request);
//        if (bearerToken.equals(jedis.get(userName))) {
        Optional<Account> optionalAccount= accountRepo.findById(id);
        Account account;
        if (optionalAccount.isPresent()) {
            account = Account.builder()
                    .id(id)
                    .fullName(accountRequest.getFullName())
                    .address(accountRequest.getAddress())
                    .linkImg(accountRequest.getLinkImg())
                    .userName(accountRequest.getUserName())
                    .passWord(passwordEncoder.encode(accountRequest.getPassWord()))
                    .role(accountRequest.getRole())
                    .build();
            accountRepo.save(account);
            return account;
       // }
        }
        return null;

    }
    public Account changePassword(String id, ChangePasswordAccountRequest changePasswordAccountRequest){
        Optional<Account> optionalAccount= accountRepo.findById(id);
        Account account;
        if (optionalAccount.isPresent()){
            account= Account.builder().id(id)
                    .role(optionalAccount.get().getRole())
                    .fullName(optionalAccount.get().getFullName())
                    .address(optionalAccount.get().getAddress())
                    .userName(optionalAccount.get().getUserName())
                    .passWord(passwordEncoder.encode(changePasswordAccountRequest.getPassWord()))
                    .linkImg(optionalAccount.get().getLinkImg()).build();
            return accountRepo.save(account);
        }
        return null;
    }
    public Account register(AccountRequest  accountRequest) {
        Boolean check =false;
        if (accountRepo.existsAccountByUsername(accountRequest.getUserName()))
        {
            check= true;
        }

        if (check==false) {
            Account account = Account.builder().id(null).fullName(accountRequest.getFullName())
                    .address(accountRequest.getAddress())
                    .linkImg(accountRequest.getLinkImg())
                    .userName(accountRequest.getUserName())
                    .passWord(passwordEncoder.encode(accountRequest.getPassWord()))
                    .role(accountRequest.getRole())
                    .build();

            return accountRepo.save(account);
             } else return null;

    }

//    public String showToken(){
//        return jedis.get("key");
//        // return (String) redisTemplate.opsForValue().get("key");
//    }
    public String callApiKillToken() throws JsonProcessingException {
        restTemplate= new RestTemplate();
        HttpServletRequest request= (HttpServletRequest) getHeaders("Authorization");
        String userName= customJwtAuthenticationFilter.returnUsernameFromToken(request);
        HttpHeaders headers= getHeaders("Authorization");
        KillTokenRequest killTokenRequest= new KillTokenRequest(userName);
        String bodyKillToken= getBody(killTokenRequest);
        HttpEntity<String> killTokenEntity= new HttpEntity<>(bodyKillToken,headers);
        ResponseEntity<String> postRequet=restTemplate.exchange("http://localhost:8080/authen/kill-token",HttpMethod.POST,killTokenEntity,String.class);
        return postRequet.getBody();
    }
    private HttpHeaders getHeaders(String authorization) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        return headers;
    }

    private String getBody(final Object o) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(o);
    }




}
