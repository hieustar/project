package com.example.account.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author hieuvm
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document("account")
@Builder
public class Account {
    @Id
    String id;
    @Field("full_name")
    String fullName;
    @Field("address")
    String address;
    @Field("link_img")
    String linkImg;
    @Field("username")
    String userName;
    @Field("password")
    String passWord;
    @Field("role")
    String role;

    public Account(String fullName, String address, String linkImg, String userName, String passWord, String role) {
        this.fullName = fullName;
        this.address = address;
        this.linkImg = linkImg;
        this.userName = userName;
        this.passWord = passWord;
        this.role = role;
    }
}
