package com.example.account.Repository;

import com.example.account.db.Account;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author hieuvm
 */
@Repository
public interface AccountRepo extends MongoRepository<Account,String> {
    @Query(value = "{ 'username' : ?0,'password' :?0}")
    Account findByUsernameAndPassWord(String username,String password);
    @Query(value = "{ 'username' : ?0}")
    Account findByUsername(String username);
    @Query(value = "{ 'username' : ?0}")
    Boolean existsAccountByUsername(String username);
}
