package com.example.account.cache;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

/**
 * @author hieuvm
 */
@Service
public class CacheToken {
    Jedis jedis= new Jedis();
    public void saveCache(String key,String value){
        jedis.set(key,value);
    }
    public String getCache(String key){
        String value= jedis.get(key);
        return value;
    }
    public void deleteCacheByKey(String key){
        jedis.del(key);
    }
    public void deleteAllCache(){
        jedis.flushAll();
        jedis.close();
    }

    public void saveCache(String extractJwtFromRequest) {
    }
}
