package com.example.account.config;

import com.example.account.config.mongo.ResourceProperties;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author hieuvm
 */
@Service
public class JwtUtil {
    @Autowired
    ResourceProperties resourceProperties;
    public boolean validateToken(String authenToken){
        try {
            Jws<Claims> claimsJws= Jwts.parser().setSigningKey(resourceProperties.getSecret()).parseClaimsJws(authenToken);
            return true;
        }catch (SignatureException | MalformedJwtException | UnsupportedJwtException | IllegalArgumentException ex) {
            throw new BadCredentialsException("INVALID_CREDENTIALS", ex);
        } catch (ExpiredJwtException ex) {
            throw ex;
        }
    }
    public String getUsernameFromToken(String token){
        Claims claims= Jwts.parser().setSigningKey(resourceProperties.getSecret()).parseClaimsJws(token).getBody();
        return claims.getSubject();
    }
    public List<SimpleGrantedAuthority> getRolesFromToken(String token){
        Claims claims= Jwts.parser().setSigningKey(resourceProperties.getSecret()).parseClaimsJws(token).getBody();
        List<SimpleGrantedAuthority> roles= null;
        Boolean isAdmin= claims.get("isAdmin",Boolean.class);
        Boolean isUser= claims.get("isUser",Boolean.class);
        if (isAdmin != null && isAdmin) {
            roles = Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }

        if (isUser != null && isUser) {
            roles = Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
        }
        return roles;
    }
}
