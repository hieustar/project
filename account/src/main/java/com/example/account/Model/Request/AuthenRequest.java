package com.example.account.Model.Request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hieuvm
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthenRequest {
    @JsonProperty("username")
    @SerializedName("username")
    String userName;
    @SerializedName("password")
    @JsonProperty("password")
    String passWord;
    @SerializedName("role")
    @JsonProperty("role")
    String role;
}
