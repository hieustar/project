package com.example.account.Controller;

import com.example.account.Model.Request.AccountRequest;
import com.example.account.Model.Request.AuthenRequest;
import com.example.account.Model.Request.LoginAccountRequest;
import com.example.account.Service.AuthenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author hieuvm
 */
@RestController
@RequestMapping("/general")
public class GeneralController {
    @Autowired
    AuthenService authenService;
    @PostMapping(path = "/login")
    public ResponseEntity<AuthenRequest> login(@RequestBody LoginAccountRequest loginRequest) throws Exception {
        return authenService.login(loginRequest);
    }

//    @PostMapping(path = "/show-token")
//    public String showToken(){
//        return authenService.showToken();
//    }
    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody AccountRequest accountRequest){
        return ResponseEntity.ok(authenService.register(accountRequest));
    }
    @GetMapping(path = "/get-all")
    public List getAll(){
        return authenService.getAll();
    }
}
