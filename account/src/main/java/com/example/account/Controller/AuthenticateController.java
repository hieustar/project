package com.example.account.Controller;

import com.example.account.Model.Request.AccountRequest;
import com.example.account.Service.AuthenService;
import com.example.account.db.Account;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hieuvm
 */
@RestController
@RequestMapping("/authenticate")
public class AuthenticateController {
    @Autowired
    AuthenService authenService;
    @GetMapping(path = "/get-all")
    public List getALL(HttpServletRequest request,
                       @RequestHeader("Authorization") String header){
        //System.out.println(header);
        return authenService.getAll();
    }
    @PutMapping(path = "/update/{id}")
    public Account update(@PathVariable("id") String id, @RequestBody AccountRequest accountRequest,HttpServletRequest request){
        return authenService.update(id,accountRequest);
    }
    @PostMapping(path = "/kill-token")
    public String apiKillToken() throws JsonProcessingException {
       return authenService.callApiKillToken();
    }
    @PostMapping(path = "test")
    public String test(){
        return "test";
    }

}
